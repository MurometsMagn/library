package org.example;

import java.util.ArrayList;
import java.util.List;

public class BookInstance {
    private Book book;
    private BookQuality quality;
    private final List<BookIssue> issues = new ArrayList<>();

    public BookInstance(Book book, BookQuality quality) {
        this.book = book;
        this.quality = quality;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public BookQuality getQuality() {
        return quality;
    }

    public void setQuality(BookQuality quality) {
        this.quality = quality;
    }

//    public List<BookIssue> getIssues() {
//        return issues;
//    }
//
//    public void setIssues(List<BookIssue> issues) {
//        this.issues = issues;
//    }

    public BookIssue getIssue(int index) {
        return issues.get(index);
    }

    public int getIssueQuantity() {
        return issues.size();
    }

    public void addIssue(BookIssue bookIssue) {
        issues.add(bookIssue);
    }
}
