package org.example;

public class BookCase {
    //BookInstance[][] //двухмерный массив ссылок на экземпляры книги [номер полки][номер книги на полки]
    private final BookInstance[][] bookInstances;
    private int bookCaseNumber;
    private int bookShelves;
    private int shelfSize;

    public BookCase(int bookCaseNumber, int bookShelves, int shelfSize) {
        this.bookCaseNumber = bookCaseNumber;
        this.bookShelves = bookShelves;
        this.shelfSize = shelfSize;
        bookInstances = new BookInstance[bookShelves][shelfSize];
    }

//    public BookInstance[][] getBookInstances() {
//        return bookInstances;
//    }
//
//    public void setBookInstances(BookInstance[][] bookInstances) {
//        this.bookInstances = bookInstances;
//    }

    public BookInstance getBookInstance(int shelfNumber, int bookNumber) {
        return bookInstances[shelfNumber][bookNumber];
    }

    public void setBookInstance(int shelfNumber, int bookNumber, BookInstance bookInstance) {
        bookInstances[shelfNumber][bookNumber] = bookInstance;
    }

    public int getBookCaseNumber() {
        return bookCaseNumber;
    }

    public void setBookCaseNumber(int bookCaseNumber) {
        this.bookCaseNumber = bookCaseNumber;
    }

    public int getBookShelves() {
        return bookShelves;
    }

    public int getShelfSize() {
        return shelfSize;
    }
}
