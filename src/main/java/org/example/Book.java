package org.example;

import java.util.ArrayList;
import java.util.List;

public class Book {
    private final List<Author> authors;
    private String name;
    private final List<Topic> topics = new ArrayList<>();
    private int pageQuantity;
    private final List<BookInstance> bookInstances = new ArrayList<>();

    public Book(List<Author> authors, String name, int pageQuantity) {
        this.authors = authors;
        this.name = name;
        this.pageQuantity = pageQuantity;
    }

    //enum Topic {}

//    public List<Author> getAuthors() {
//        return authors;
//    }
//
//    public void setAuthors(List<Author> authors) {
//        this.authors = authors;
//    }

    public Author getAuthor(int index) {
        return authors.get(index);
    }

    public int getAuthorQuantity() {
        return authors.size();
    }

    public void addAuthor(Author author) {
        authors.add(author);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public List<Topic> getTopics() {
//        return topics;
//    }
//
//    public void setTopics(List<Topic> topics) {
//        this.topics = topics;
//    }

    public Topic getTopic(int index) {
        return topics.get(index);
    }

    public int topicQuantity() {
        return topics.size();
    }

    public void addTopic(Topic topic) {
        topics.add(topic);
    }

    public int getPageQuantity() {
        return pageQuantity;
    }

    public void setPageQuantity(int pageQuantity) {
        this.pageQuantity = pageQuantity;
    }

//    public List<BookInstance> getBookInstances() {
//        return bookInstances;
//    }
//
//    public void setBookInstances(List<BookInstance> bookInstances) {
//        this.bookInstances = bookInstances;
//    }

    public BookInstance getBookInstance(int index) {
        return bookInstances.get(index);
    }

    public int getBookInstanceQuantity() {
        return bookInstances.size();
    }

    public void addBookInstance(BookInstance bookInstance) {
        bookInstances.add(bookInstance);
    }
}
