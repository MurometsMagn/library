package org.example;

import java.util.Date;

public class BookIssue {
    private BookInstance bookInstance;
    private Reader reader;
    private Date issueDate;
    private Date dueDate;
    private Date returnDate = null;

    public BookIssue(BookInstance bookInstance, Reader reader, Date issueDate, Date dueDate) {
        this.bookInstance = bookInstance;
        this.reader = reader;
        this.issueDate = issueDate;
        this.dueDate = dueDate;
    }

    public BookInstance getBookInstance() {
        return bookInstance;
    }

    public void setBookInstance(BookInstance bookInstance) {
        this.bookInstance = bookInstance;
    }

    public Reader getReader() {
        return reader;
    }

    public void setReader(Reader reader) {
        this.reader = reader;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }
}
