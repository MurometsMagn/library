package org.example;

import java.util.ArrayList;
import java.util.List;

public class Reader {
    private String firstName;
    private String middleName;
    private String lastName;
    private final List<BookIssue> issues = new ArrayList<>();

    public Reader(String firstName, String middleName, String lastName) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

//    public List<BookIssue> getIssues() {
//        return issues;
//    }
//
//    public void setIssues(List<BookIssue> issues) {
//        this.issues = issues;
//    }

    public BookIssue getIssues(int index) {
        return issues.get(index);
    }

    public int getIssueQuantity() {
        return issues.size();
    }

    public void addIssue(BookIssue bookIssue) {
        issues.add(bookIssue);
    }
}
