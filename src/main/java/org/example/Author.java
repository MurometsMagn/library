package org.example;

import java.util.ArrayList;
import java.util.List;

public class Author {
    private String name;
    private final List<Book> books = new ArrayList<>();

    public Author(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public List<Book> getBooks() {
//        return books;
//    }

    public Book getBook(int numberOfBook) {
        return books.get(numberOfBook);
    }

    public int getBookQuantity() {
        return books.size();
    }

    public void addBook(Book book) {
        books.add(book);
    }

//    public void setBooks(List<Book> books) {
//        this.books = books;
//    }
}
